create schema sel;

create table if not exists sel.data_items (
  id serial primary key,
  type text not null,
  name text not null,
  vicinity text default null,
  lat float not null,
  lng float not null
  ext_place_id text not null,
);
