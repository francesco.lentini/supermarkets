import psycopg2
import sys
import json

filename = sys.argv[1]

def readFile(path):
    with open(path) as f:
        data = json.load(f)
        return data

def getRecord(datum):
    return {
        "type": "supermarket",
        "name": datum['name'],
        "vicinity": datum['vicinity'],
        "lat": datum['geometry']['location']['lat'],
        "lng": datum['geometry']['location']['lng'],
        "ext_place_id": datum['place_id']
    }

def formatString(s):
    return s.replace("'", "''")

dict = readFile(filename)

print("Results", len(dict['results']))

try:
    connection = psycopg2.connect(user="postgres", password="postgres", host="127.0.0.1", database="supermercati")
    cursor = connection.cursor()

    for r in dict['results']:
        record = getRecord(r)

        statement = "INSERT INTO sel.data_items (type, name, vicinity, lat, lng, ext_place_id) VALUES ('{}', '{}', '{}', {}, {}, '{}');".format(record['type'], formatString(record['name']), formatString(record['vicinity']), record['lat'], record['lng'], record['ext_place_id'])

        cursor.execute(statement)

    connection.commit()

except (Exception, psycopg2.Error) as error:
    print("Error connecting to Postgres", error)
finally:
    if(connection):
        cursor.close()
        connection.close()
        print("Postgres connection closed")

