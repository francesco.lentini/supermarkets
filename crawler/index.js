const needle = require('needle');
const fs = require('fs');
const chalk = require('chalk');
const uuid = require('uuid').v1;

const startingUrl = process.env.URL || fs.readFileSync('./url.txt').toString().trim();
const [id] = uuid().split('-');
const { log } = console;

const output = `./output/${id}`;

let counter = 0;

if (!fs.existsSync()) {
	fs.mkdirSync(output);
}

const getData = (url) => {
	log(chalk.green(`[${chalk.bold(id)}] Chiamo ${chalk.red(url)} - pagina ${counter}\n`));

	needle.get(url, (err, response) => {
		if (err || response.statusCode !== 200) {
			log(chalk.white.bgRed(`No bel. ${chalk.bold('status code')}: ${response.statusCode}`), err);
			process.exit(1);
		}

		fs.writeFileSync(`${output}/page-${counter++}.json`, JSON.stringify(response.body));

		if (response.body.next_page_token) { 
			setTimeout(() => {
				getData(`${startingUrl}&pagetoken=${response.body.next_page_token}`);	
			}, parseInt(process.env.INTERVAL, 10) || 5000);
		}
	})
}

getData(startingUrl);
